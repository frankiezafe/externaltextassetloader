# README #

Code to load text files from the disk, in edition and at run time.
More info: http://lab.frankiezafe.org/index.php?id=250

**Source code *ExternalAssetLoader.cs* **


// ExternalAssetLoader.cs

using UnityEngine;

using System;

using System.Threading;

public class ExternalAssetLoader {
    
    // retrieval of the absolute path & cleanup
    private static string absolutePath = "";
    public static string getAbsolutePath() {
        if ( absolutePath.Equals( "" ) ) {
            string localp = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            // edit mode
            if ( localp.IndexOf( "/Library/ScriptAssemblies/" ) > -1 ) {
                absolutePath = localp.Substring( 0, localp.IndexOf( "/Library/ScriptAssemblies/" ) ) + "/";
            // build mode, path to [YOUR PROJECT]_Data folder
            } else if ( localp.IndexOf( "/Managed/" ) > -1 ) {
                absolutePath = localp.Substring( 0, localp.IndexOf( "/Managed/" ) ) + "/";
            } else {
                Debug.LogError( "Impossible to deduce the absolute path! " + localp );
            }
        }
        return absolutePath;
    }

    // extraction of the json content (or any text file )
    public static string loadExternalText( string relative_path ) {
        string lpath = ExternalAssetLoader.getAbsolutePath () + relative_path;
        WWW line_configs = new WWW( lpath );
        if ( line_configs == null ) {
            Debug.LogError ( "B2Jutils::loadExternalJson " + lpath + " not found" );
            return "";
        }
        int waiter = 1000;
        while( !line_configs.isDone && waiter > 0 ) {
            Thread.Sleep( 1 );
            waiter--;
        }
        return System.Text.ASCIIEncoding.ASCII.GetString( line_configs.bytes );
    }

}


**Source code *Example.cs* **


// Example.cs

using UnityEngine;

using System.Collections;

using System.Collections.Generic;

using ExternalAssetLoader;

using MiniJSON;

public class Consumer : MonoBehaviour
{

    void Start()
    {
        string path = "relative/path/in/my/project/folder/data.json";
        string json = ExternalAssetLoader.loadExternalJson(path);
        IDictionary data = (IDictionary)Json.Deserialize(json);
        if (data == null)
        {
            Debug.LogError("Failed to parse " + path);
            return;
        }
        Debug.log(json);

    }
}

### Who do I talk to? ###

* Fran�ois Zaj�ga - http://www.frankiezafe.org