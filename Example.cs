using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using ExternalAssetLoader;
using MiniJSON;

public class Consumer : MonoBehaviour
{

    void Start()
    {
        string path = "relative/path/in/my/project/folder/data.json";
        string json = ExternalAssetLoader.loadExternalJson(path);
        IDictionary data = (IDictionary)Json.Deserialize(json);
        if (data == null)
        {
            Debug.LogError("Failed to parse " + path);
            return;
        }
        Debug.log(json);

    }
}