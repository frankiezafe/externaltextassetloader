using UnityEngine;

using System;
using System.Threading;

public class ExternalAssetLoader
{

    // retrieval of the absolute path & cleanup
    private static string absolutePath = "";
    public static string getAbsolutePath()
    {
        if (absolutePath.Equals(""))
        {
            string localp = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            // edit mode
            if (localp.IndexOf("/Library/ScriptAssemblies/") > -1)
            {
                absolutePath = localp.Substring(0, localp.IndexOf("/Library/ScriptAssemblies/")) + "/";
                // build mode, path to [YOUR PROJECT]_Data folder
            }
            else if (localp.IndexOf("/Managed/") > -1)
            {
                absolutePath = localp.Substring(0, localp.IndexOf("/Managed/")) + "/";
            }
            else
            {
                Debug.LogError("Impossible to deduce the absolute path! " + localp);
            }
        }
        return absolutePath;
    }

    // extraction of the json content (or any text file )
    public static string loadExternalText(string relative_path)
    {
        string lpath = ExternalAssetLoader.getAbsolutePath() + relative_path;
        WWW line_configs = new WWW(lpath);
        if (line_configs == null)
        {
            Debug.LogError("B2Jutils::loadExternalJson " + lpath + " not found");
            return "";
        }
        int waiter = 1000;
        while (!line_configs.isDone && waiter > 0)
        {
            Thread.Sleep(1);
            waiter--;
        }
        return System.Text.ASCIIEncoding.ASCII.GetString(line_configs.bytes);
    }

}